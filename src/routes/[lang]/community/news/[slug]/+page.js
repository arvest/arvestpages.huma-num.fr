import { lang } from '$lib/scripts/stores.js';

let langVal;
lang.subscribe(value => {
    langVal = value;
});

export async function load({ params }) {
    const { slug } = params;
    
    try{
        const article = await import(`../../../../../content/news/${slug}/${langVal}.svx`);
        const this_slug = slug
        return { article, this_slug };
    }catch(error){
        try{
            const article = await import(`../../../../../content/news/${slug}/en.svx`);
            const this_slug = slug
            return { article, this_slug };
        }catch(error){
            console.log(error)
        }
    }
}