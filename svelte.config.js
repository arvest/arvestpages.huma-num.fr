import adapter from '@sveltejs/adapter-static';
import { mdsvex } from 'mdsvex';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	extensions: ['.svelte', '.svx'], 

	preprocess: mdsvex({
		extensions: ['.svx']
	}),
	
	kit: {
		// adapter-auto only supports some environments, see https://kit.svelte.dev/docs/adapter-auto for a list.
		// If your environment is not supported, or you settled on a specific environment, switch out the adapter.
		// See https://kit.svelte.dev/docs/adapters for more information about adapters.
        adapter: adapter({
            fallback: 'index.html',
			pages: 'build',
      		assets: 'build',
        }),
		prerender: {
			entries: ['*'] // Prerender all routes
		},
		paths : {
			base: process.env.NODE_ENV === 'production' ? '' : '',
		}
	}
};

export default config;
